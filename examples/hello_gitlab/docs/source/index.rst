.. hello documentation master file, created by
   sphinx-quickstart on Wed Feb 14 20:57:22 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to hello's documentation!
=================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   api

Installation
============

.. code-block:: console
   
   pip install hello

Usage
=====

.. code-block:: python

   from hello import hello_world
   
   hello_world()
   hello_world('my message')

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
