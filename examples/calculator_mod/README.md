# Comment distribuer le package sur PyPI (ou Test PyPI)

## Construire la distribution

Nous utilisons l'outil [build](https://build.pypa.io/en/stable/) pour construire la distribution de notre package.

Il peut être installer par pip:

    pip install build

La command pour créer notre distribution est:

    python -m build

## Distribuer sur PyPI (ou Test PyPI)

L'outil `build` génère les distributions dans le dossier dist (par défaut).
Nous pouvons ensuite les distribuer grâce à l'outil [twine](https://twine.readthedocs.io/en/stable/).

Il peut ếtre installer par pip:

    pip install twine

La command pour mettre nos distributions sur Test PyPI est:

    twine upload -r testpypi dist/*

La command pour mettre nos distributions sur PyPI est:

    twine upload dist/*

La command `upload` vous demandera vos identifiants de vos comptes (PyPI ou Test PyPI).
Avant de les distribuer il est recommander de les vérifier.

    twine check dist/*
