from __future__ import annotations
import cowsay

def cow_display(message: str) -> None:
    """
    display cow saying the message

    Parameters
    ----------
    message : str
        a message to be displayed
    """
    cowsay.cow(message)