from __future__ import annotations
from typing import Optional

from importlib.metadata import entry_points

try :
    display = entry_points(group='hello.display', name='cow')[0].load()
except IndexError:
    def display(message: str) -> None:
        print(f'!!! {message} !!!')

def hello_world(message : Optional[str] = 'Hello world') -> None:
    """
    display a message

    Parameters
    ----------
    message : Optional[str], optional
        The message to print, by default 'Hello world'
    """
    display(message)