#include <iostream>
#include <string>
#include <pybind11/pybind11.h>

void hello_world(const std::string& message = "Hello world") 
{
    std::cout << message << std::endl;
}

using namespace pybind11::literals;

PYBIND11_MODULE(world, m) {
    m.doc() = "world module"; // optional module docstring
    m.def("hello_world", &hello_world, "message"_a = "Hello World", R"(
        display a message

        Parameters
        ----------
        message : Optional[str], optional
            The message to print, by default 'Hello world'
    )");
}