from setuptools import setup
from setuptools.extension import Extension
from pathlib import Path

if Path('src/calculator/cython_mod.c').exists():
    extension = [
        Extension(
            name = 'calculator.cython_mod',
            sources = ['src/calculator/cython_mod.c']
        )
    ]
else:
    from Cython.Build import cythonize
    extension = cythonize([
        Extension(
            name = 'calculator.cython_mod',
            sources = ['src/calculator/cython_mod.pyx']
        )
    ])

setup(
    ext_modules=extension,
)

