"""
try calculator module using the command line
"""
import sys
from .parser import parser

def main():
    """
    compute input line
    """
    s = "2.56+5*9/1+9*7" if len(sys.argv) == 1 else "".join(sys.argv[1:])
    print(parser(s.replace(" ", "")))
