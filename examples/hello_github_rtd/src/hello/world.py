from __future__ import annotations
import cowsay
from typing import Optional

def hello_world(message : Optional[str] = 'Hello world') -> None:
    """
    print a cow saying a message

    Parameters
    ----------
    message
        The message to print
    """
    cowsay.cow(message)