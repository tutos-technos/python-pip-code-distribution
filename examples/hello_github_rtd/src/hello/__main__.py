import sys
from hello import hello_world

def main():
    if len(sys.argv) == 1:
        hello_world()
    else:
        hello_world(' '.join(sys.argv[1:]))

if __name__ == '__main__':
    main()