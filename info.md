[Distribuer son application Python](https://gouarin.github.io/python-packaging-2023/)

[Python Packaging User Guide](https://packaging.python.org/en/latest/)

[Distribuer son application Python - code source](https://github.com/gouarin/python-packaging-2023)

[toml doc](https://toml.io/en/v1.0.0)

[setuptools](https://setuptools.pypa.io/en/latest/userguide/index.html)
